import React, { useState } from 'react';
import { NavLink } from 'react-router-dom';

function Nav() {
  const [isAnimeSearchHovered, setIsAnimeSearchHovered] = useState(false);
  const [isHistoryHovered, setIsHistoryHovered] = useState(false)

  const styleAnimeSearch = {
    transition: 'font-size 0.3s',
    fontSize: isAnimeSearchHovered ? '1.4em' : '1.2em',
  };

  const styleSearchHistory = {
    transition: 'font-size 0.3s',
    fontSize: isHistoryHovered ? '1.4em' : '1.2em',
  };

  const onMouseEnterSearch = () => {
    setIsAnimeSearchHovered(true);
  }

  const onMouseLeaveSearch = () => {
    setIsAnimeSearchHovered(false);
  }

  const onMouseEnterHistory = () => {
    setIsHistoryHovered(true);
  }

  const onMouseLeaveHistory = () => {
    setIsHistoryHovered(false);
  }

  const customBlue = "#3B87BC";
  
  return (
    <nav className="navbar navbar-expand-lg navbar-dark" style={{ backgroundColor: customBlue }}>
      <div className="container-fluid">
        <div className="d-flex justify-content-start align-items-center flex-grow-1">
          <h1 className="navbar-brand fs-4">AniWhat</h1>
        </div>
        <NavLink
          className="navbar-brand"
          to="/AnimeSearchForm/"
          onMouseEnter={onMouseEnterSearch}
          onMouseLeave={onMouseLeaveSearch}
          style={{ ...styleAnimeSearch, backgroundColor: customBlue }}
        >
          Anime Search
        </NavLink>
        <NavLink
          className="navbar-brand"
          to="/AnimeList/"
          onMouseEnter={onMouseEnterHistory}
          onMouseLeave={onMouseLeaveHistory}
          style={{ ...styleSearchHistory, backgroundColor: customBlue }}
        >
          Search History
        </NavLink>
        <button
          className="navbar-toggler"
          type="button"
          data-bs-toggle="collapse"
          data-bs-target="#navbarSupportedContent"
          aria-controls="navbarSupportedContent"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0"></ul>
        </div>
      </div>
    </nav>
  );
}

export default Nav;
