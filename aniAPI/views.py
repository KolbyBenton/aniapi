from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
from .models import Anime
from .encoders import AnimeEncoder
from django.views.decorators.csrf import csrf_exempt
import requests
import json


@csrf_exempt
@require_http_methods(["GET", "POST"])
def search_anime(request):
    if request.method == "GET":
        animes = Anime.objects.all()
        return JsonResponse(
            {"animes": animes},
            encoder=AnimeEncoder
        )

    else:

        content = json.loads(request.body)
        image_url = content["image_url"]
        trace_moe_api_url = "https://api.trace.moe/search"
        params = {"anilist_id": 1, "url": image_url}
        response = requests.get(trace_moe_api_url, params=params).json()
        print("FULL RESPONSE", response)

        if "result" in response and response["result"]:
            anime_info = response["result"][0]
            title = anime_info["filename"]
            episode_number = anime_info["episode"]
            frm = anime_info["from"]
            to = anime_info["to"]
            video = anime_info["video"]

            print("THIS IS THE ANIME INFO", anime_info)

            answer = {
                "title": title,
                "episode_number": episode_number,
                "frm": frm,
                "to": to,
                "video": video,
            }

            anime = Anime.objects.create(**answer)

            return JsonResponse(
                anime,
                encoder=AnimeEncoder,
                safe=False
            )


@require_http_methods(["GET"])
def search_result(request, id):
    anime = Anime.objects.get(id=id)
    return JsonResponse(
        anime,
        encoder=AnimeEncoder,
        safe=False,
    )
