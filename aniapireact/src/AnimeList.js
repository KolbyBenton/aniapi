import React, { useEffect, useState } from 'react';

const AnimeList = () => {
    const [animes, setAnimes] = useState([]);
    const fetchData = async () => {
        const url = 'http://localhost:8000/api/searchanime/';
        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setAnimes(data.animes);
            console.log(data.animes)
        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    return (
        <table className="table table-striped">
        <thead>
          <tr>
            <th>Title</th>
            <th>Episode Number</th>
            <th>From</th>
            <th>To</th>
          </tr>
        </thead>
        <tbody>
          {animes.map(anime => {
            return (
              <tr key={anime.id}>
                <td> { anime.title }</td>
                <td> {anime.episode_number } </td>
                <td> {anime.frm } </td>
                <td> {anime.to } </td>
              </tr>
            );
          })}
        </tbody>
      </table>

    )
}

export default AnimeList;
