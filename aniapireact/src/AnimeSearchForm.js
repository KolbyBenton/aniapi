import React, { useState } from "react";
import 'bootstrap/dist/css/bootstrap.min.css';
import { useNavigate } from 'react-router-dom';


const AnimeSearchForm = () => {
  const [formData, setFormData] = useState({
    image_url: ''
  });
  const [error, setError] = useState('');
  const [loading, setLoading] = useState(false);
  const [searchResult, setSearchResult] = useState(null); 
  const navigate = useNavigate();
 

  const handleSubmit = async (event) => {
    event.preventDefault();

    try {
      setLoading(true);

      const animeUrl = 'http://localhost:8000/api/searchanime/';
      
      const fetchConfig = {
        method: "post",
        body: JSON.stringify(formData),
        headers: {
          'Content-Type': 'application/json',
        },
      };

      const response = await fetch(animeUrl, fetchConfig);

      if (response.ok) {
        const responseData = await response.json();
        setFormData({
          image_url: ''
        });
        setError('');
        
        setSearchResult(responseData.searchResult);

        console.log('Redirecting to /SearchResult');
        navigate(`/SearchResult/`);
      } else {
        const errorData = await response.json();
        setError(errorData.message);
        console.error('Error:', errorData.message);
      }
    } catch (error) {
      setError('An error occurred during the request.');
      console.error('An error occurred:', error);
    } finally {
      setLoading(false);
    }
  };

  const handleFormChange = (e) => {
    const value = e.target.value;
    const inputName = e.target.name;
    setFormData({
      ...formData,
      [inputName]: value
    });
  };

  const BackgroundGif = () => {
    const containerStyle = {
      backgroundImage: 'url(https://images-wixmp-ed30a86b8c4ca887773594c2.wixmp.com/f/599ce912-73bb-4d30-a758-68953151088c/dd04zg6-f0a44c93-b67d-4279-ac58-c2ce1919a09f.png?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ1cm46YXBwOjdlMGQxODg5ODIyNjQzNzNhNWYwZDQxNWVhMGQyNmUwIiwiaXNzIjoidXJuOmFwcDo3ZTBkMTg4OTgyMjY0MzczYTVmMGQ0MTVlYTBkMjZlMCIsIm9iaiI6W1t7InBhdGgiOiJcL2ZcLzU5OWNlOTEyLTczYmItNGQzMC1hNzU4LTY4OTUzMTUxMDg4Y1wvZGQwNHpnNi1mMGE0NGM5My1iNjdkLTQyNzktYWM1OC1jMmNlMTkxOWEwOWYucG5nIn1dXSwiYXVkIjpbInVybjpzZXJ2aWNlOmZpbGUuZG93bmxvYWQiXX0.CMzAVuqJAHCIaPNbIpICdjpN7nNDQ4QLm4r2NNI5L-o)',
      backgroundSize: '200px',
      backgroundRepeat: 'no-repeat',
      backgroundPosition: '30.5% 33%',
      minHeight: '100vh',
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center',
    };

    return (
      <div style={containerStyle}>
        <div className="col-6">
          <div className="shadow p-4 mt-4 text-center">
            <h1 className="text-center">Enter Image URL</h1>
            <form onSubmit={handleSubmit} id="create-conference-form">
              <div className="form-floating mb-3">
                <input
                  onChange={handleFormChange}
                  value={formData.image_url}
                  placeholder="Image Url"
                  required
                  type="text"
                  name="image_url"
                  id="image_url"
                  className="form-control"
                />
                <label htmlFor="name">Image URL</label>
              </div>
              <button className="btn btn-primary" disabled={loading}
                onClick={handleSubmit}
                style ={{ backgroundColor: "#3B87BC"}}>
                  
                {loading ? 'Searching...' : 'Search'}
              </button>
              {error && <p className="text-danger mt-3">{error}</p>}
            </form>
          </div>
        </div>
      </div>
    );
  };

  return <BackgroundGif />;
};

export default AnimeSearchForm;