import React, { useEffect, useState } from 'react';

const SearchResult = () => {
  const [animes, setAnimes] = useState([]);
  const [latestResult, setLatestResult] = useState(null);

  const fetchData = async () => {
    const url = 'http://localhost:8000/api/searchanime/';
    try {
      const response = await fetch(url);

      if (response.ok) {
        const data = await response.json();
        const latestAnime = data.animes[data.animes.length - 1];
        setAnimes(data.animes);
        setLatestResult(latestAnime);
        console.log('Latest result:', latestAnime);
      }
    } catch (error) {
      console.error('Error fetching data:', error);
    }
  };

  useEffect(() => {
    fetchData();
  }, []);

  return (
    <div>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Name</th>
            <th>Episode Number</th>
            <th>From</th>
            <th>To</th>
          </tr>
        </thead>
        <tbody>
          {latestResult && (
            <tr>
              <td>{latestResult.title}</td>
              <td>{latestResult.episode_number}</td>
              <td>{latestResult.frm}</td>
              <td>{latestResult.to}</td>
            </tr>
          )}
        </tbody>
      </table>

      {latestResult && (
        <div style={{display: 'flex', justifyContent: 'center' }}>
          <div style={{ padding: '100px' }}>
          <h2></h2>
          <video width="640" height="360" controls>
            <source src={latestResult.video} type="video/mp4" />
            Your browser does not support the video tag.
          </video>
          </div>
        </div>
      )}
    </div>
  );
};

export default SearchResult;
