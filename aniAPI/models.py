from django.db import models


class Anime(models.Model):
    title = models.CharField(max_length=255)
    episode_number = models.IntegerField()
    frm = models.FloatField()
    to = models.FloatField()
    video = models.URLField(max_length=500)