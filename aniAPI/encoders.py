from .common.json import ModelEncoder
from .models import Anime


class AnimeEncoder(ModelEncoder):
    model = Anime
    properties = [
        "title",
        "episode_number",
        "frm",
        "to",
        "video",
    ]
