from django.urls import path
from .views import search_anime, search_result

urlpatterns = [
    path('searchanime/', search_anime, name='search_anime'),
    path('searchresult/<int:id>/', search_result, name='search_result')
]
