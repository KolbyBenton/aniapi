import { BrowserRouter, Routes, Route } from 'react-router-dom';
import Nav from './Nav';
import AnimeList from './AnimeList'
import AnimeSearchForm from './AnimeSearchForm'
import SearchResultPage from './SearchResultPage'
import './App.css';

function App() {
    return (
      <BrowserRouter>
        <Nav />
        <div className="container">
          <Routes>
            <Route path="/AnimeSearchForm" element={<AnimeSearchForm />} />
            <Route path="/AnimeList" element={<AnimeList />} />
            <Route path="/SearchResult" element={<SearchResultPage />} />
          </Routes>
        </div>
      </BrowserRouter>
    );
  }

  export default App;
